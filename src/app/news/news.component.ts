import { Component, OnInit } from '@angular/core';
import { form2 } from '../form';
import {ProductService} from '../product.service';
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  form: form2[];
  form1: form2[];
  index: number;
  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.productService.getProductsSmall().then(products => {
      this.form = products.slice(products.length-3, products.length);
    });
    this.productService.getProductsSmall().then(products => {
      this.form1 = products;
    });
    this.index = 0;
  }
  onClick(){
    this.index++;
    this.index%=this.form1.length;
  }
  onClick1(){
    this.index+=this.form1.length-1;
    this.index%=this.form1.length;
  }
}
