
import {AfterContentInit, AfterViewInit, Component, ElementRef, OnInit, TemplateRef, ViewChild,} from '@angular/core';
import { form2 } from './form';
import {ProductService} from './product.service';
import {Template} from '@angular/compiler/src/render3/r3_ast';
import {colors} from '@angular/cli/utilities/color';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent implements OnInit, AfterViewInit {
  title = 'FirstPrimeNgProject';
  form: form2[];
  responsiveOptions;
  constructor(private productService: ProductService) {
    this.responsiveOptions = [
      {
        breakpoint: '1024px',
        numVisible: 2,
        numScroll: 2
      },
      {
        breakpoint: '768px',
        numVisible: 1,
        numScroll: 1
      },
      {
        breakpoint: '560px',
        numVisible: 1,
        numScroll: 1
      }
    ];
  }
  @ViewChild("vn") tp: ElementRef
  ngOnInit() {
    this.productService.getProductsSmall().then(products => {
      this.form = products;
    });

  }
  ngAfterViewInit() {
    console.log(this.tp.nativeElement.style.display)
  }
}
