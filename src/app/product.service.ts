import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { form2 } from './form';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private http: HttpClient) { }

  getProductsSmall() {
    return this.http.get<any>('assets/new.json')
        .toPromise()
        .then(res => <form2[]>res.data)
        .then(data => { return data; });
  }


}
