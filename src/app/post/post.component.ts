import { Component, OnInit } from '@angular/core';
import { form2 } from '../form';
import {ProductService} from '../product.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
    form: form2[];
  responsiveOptions;
  count;
  constructor(private productService: ProductService) {
    this.responsiveOptions = [
      {
        breakpoint: '1024px',
        numVisible: 2,
        numScroll: 2
      },
      {
        breakpoint: '768px',
        numVisible: 1,
        numScroll: 1
      },
      {
        breakpoint: '560px',
        numVisible: 1,
        numScroll: 1
      }
    ];

  }
  uploadedFiles: any[] = [];
  khanh: any;
  ngOnInit() {
    this.productService.getProductsSmall().then(products => {
      this.form = products;
      this.count = products.length;
    });
  }
  onUpload(event) {
    for (let file of event.files) {
      this.uploadedFiles.push(file);
    }
  }
}
